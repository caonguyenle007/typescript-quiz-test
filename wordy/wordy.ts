const containNumberInStringRegex = /\d+/;
const operationSpliceRegex = /(-?\d+)(\s(plus|minus|multiplied by|divided by|raised to the)\s(-?\d+)(th\spower)?)+/gi;

export class ArgumentError {
    constructor() {}
}

export class WordProblem {
    question: string;
    constructor(question: string) {
        this.question = question;
    }
    
    answer = (): ArgumentError|Number => {
        const checkExitNumber = containNumberInStringRegex.test(this.question);
        const extractResult = this.question.match(operationSpliceRegex);
        if (!checkExitNumber || !extractResult) {
            throw new ArgumentError;
        }
        let operationInString = extractResult[0];
        operationInString = operationInString
            .replace(/plus/g, '+')
            .replace(/minus/g, '-')
            .replace(/multiplied by/g, '*')
            .replace(/divided by/g, '/')
            .replace(/raised to the/g, '^')
            .replace(/th power/g, '');
        const stringInArraySplit = operationInString.split(' ');
        let numbers : number[] = [];
        let operators: string[] = [];
        stringInArraySplit.forEach(item => {
            if (!Number.parseInt(item)) {
                operators.push(item);
            } else {
                numbers.push(parseInt(item));
            }
        });
        let evaluateResult = numbers[0];
        for (let i=1; i < numbers.length; i++) {
            switch(operators[i-1]) {
                case '+':
                    evaluateResult += numbers[i];
                    break;
                case '-':
                    evaluateResult -= numbers[i];
                    break;
                case '*':
                    evaluateResult *= numbers[i];
                    break;
                case '/':
                    evaluateResult /= numbers[i];
                    break;
                case '^':
                    evaluateResult = Math.pow(evaluateResult, numbers[i]);
                    break;
                default:
                    break;
            }
        }
        
        return evaluateResult;
    };
}