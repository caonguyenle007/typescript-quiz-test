const isEven = (number: Number) => Number(number) % 2 === 0;

class CollatzConjecture {
    static steps(number: Number) : Number|String {
        if (number <= 0) {
            throw new Error("Only positive numbers are allowed");
        }
        let n = number;
        let steps = 0;
        
        while (n > 1) {
            steps = steps + 1;

            n = isEven(n) ? (Number(n) / 2) : (3 * Number(n) + 1);
        }

        return steps;
    };
}

export default CollatzConjecture