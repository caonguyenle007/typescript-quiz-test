const invalidGameRuleMsg01 = 'Pins must have a value from 0 to 10';
const invalidGameRuleMsg02 = 'Score cannot be taken until the end of the game';
const invalidGameRuleMsg03 = 'Pin count exceeds pins on the lane';
const invalidGameRuleMsg04 = 'Should not be able to roll after game is over';

export default class Bowling {
    rolls: number[] = [];
    constructor(rolls: Array<number>) {
        this.rolls = rolls;
    }

    checkGameRules = () => {
        const totalTurnThrows = this.rolls.length;
        if (totalTurnThrows === 0 || totalTurnThrows < 10) {
            throw Error(invalidGameRuleMsg02);
        }

        const invalidScoreBeforeParse = this.rolls.filter(roll => (roll < 0 || roll > 10));
        if (invalidScoreBeforeParse.length > 0) {
            throw Error(invalidGameRuleMsg01)
        }
        // parse rolls to each frame
        let i = 0;
        const rollsParseToFrames = [];
        while (i < 10) {
            if (i < 9) {
                if (this.rolls[(2 * i)] === 10 ) {
                    rollsParseToFrames.push([this.rolls[(2 * i)], 0]);
                    // put zero next to the item
                    this.rolls.splice((2 * i + 1), 0, 0);
                } else {
                    rollsParseToFrames.push([this.rolls[(i * 2)], this.rolls[(i * 2) + 1]]);
                }
            } else {
                rollsParseToFrames.push(this.rolls.splice((2 * i)));
            }

            i++;
        }
        let sumPoints = 0;
        rollsParseToFrames.forEach((frame, index) => {
            let points = frame.reduce((acc, val) => { return acc + val; }, 0);
            // Does not apply for last frame
            if (index < 9 && points > 10) {
                throw Error(invalidGameRuleMsg03);
            }
            if (index === 9) {
                // frame 10 have bonus rolls
                // If length === 3 mean the first roll in final frame is Strike
                if (frame.length === 3) {
                    if (frame[0] === 10 && frame[1] !== 10 && (points - 10) > 10) {
                        throw Error(invalidGameRuleMsg03);
                    }
                    if (points === 0) {
                        throw Error(invalidGameRuleMsg04);
                    }
                } else {
                    if (sumPoints === 0 && (points === 10 || points === 20)) {
                        throw Error(invalidGameRuleMsg02);
                    }
                }

                sumPoints += points;
            }
        })

        return rollsParseToFrames;
    }

    frameIsStrike = (frame: Array<number>): Boolean => {
        return frame[0] === 10;
    }

    frameIsSpare = (frame: Array<number>): Boolean => {
        return frame[0] !== 10 && (frame[0] + frame[1] === 10);
    }

    calculateScore = (rollsParseToFrames: Array<any>): number => {
        let totalScore = 0;
        for(let i = 0; i < rollsParseToFrames.length; i ++) {
            let frameScore = 0;
            let frame = rollsParseToFrames[i];
            if (i < 9) {
                let nextOneFrame = rollsParseToFrames[i+1];
                let nextTwoFrame = rollsParseToFrames[i+2];
                // 10 plus the number of pins knocked down in the next two throws
                if (this.frameIsStrike(frame)) {
                    frameScore = 10;
                    if (this.frameIsStrike(nextOneFrame)) {
                        frameScore += 10 + nextTwoFrame[0];
                    } else {
                        frameScore += nextOneFrame[0] + nextOneFrame[1];
                    }
                } else {
                    // 10 plus the number of pins knocked down in the next throws
                    if (this.frameIsSpare(frame)) {
                        frameScore = 10 + nextOneFrame[0];
                    } else {
                        frameScore += frame[0] + frame[1];
                    }
                }
            }
            if (i === 9) {
                if (frame.length === 3) {
                    if (this.frameIsStrike(frame) || this.frameIsSpare(frame)) {
                        frameScore += frame[0] + frame[1] + frame[2];
                    }
                } else {
                    frameScore += frame[0] + frame[1];
                }
            }

            totalScore += frameScore;
        }

        return totalScore;
    };

    score = () => {
        // check case all of frame is strike + 2
        const scoreEqualTen = this.rolls.filter(roll => roll === 10);
        if (this.rolls.length === 12 && scoreEqualTen.length === 12) {
            return 300;
        }

        const rollsParseToFrames = this.checkGameRules();

        return this.calculateScore(rollsParseToFrames);
    };
}